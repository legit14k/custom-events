﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/// <summary>
/// Jason Grimberg
/// CE05
/// User input dialog window
/// </summary>
namespace JasonGrimberg_CE05
{
    public partial class UserInputDialog : Form
    {
        // -----------------------------------------------------------------------------
        // EventHandler call
        // -----------------------------------------------------------------------------
        // Creating a new instance of the main window
        MainWindow main = new MainWindow();

        // Add a delegate
        public EventHandler ObjectAdded;

        // -----------------------------------------------------------------------------
        // Ship data components
        // -----------------------------------------------------------------------------
        // Property for modifying and extracting the text-box values
        public Ships ShipData
        {
            get
            {
                // Getting a new instance of a ship
                Ships s = new Ships();
                s.Name = tbName.Text;
                s.Cruiser = rbCruiser.Checked;
                s.Destroyer = rbDestroyer.Checked;
                s.Freighter = rbFreighter.Checked;
                s.CrewSize = numCrewSize.Value;
                s.ActiveDuty = cbActiveDuty.Checked;

                // Set the image index for what ship is selected
                if (rbCruiser.Checked == true)
                {
                    s.ImageIndex = 0;
                }
                else if (rbDestroyer.Checked == true)
                {
                    s.ImageIndex = 1;
                }
                else if (rbFreighter.Checked == true)
                {
                    s.ImageIndex = 2;
                }
                // Return the new ship
                return s;
            }
            set
            {
                // Setting the new values from the form
                tbName.Text = value.Name;
                rbCruiser.Checked = value.Cruiser;
                rbDestroyer.Checked = value.Destroyer;
                rbFreighter.Checked = value.Freighter;
                numCrewSize.Value = value.CrewSize;
                cbActiveDuty.Checked = value.ActiveDuty;
            }
        }

        // -----------------------------------------------------------------------------
        // Initialization of the component
        // -----------------------------------------------------------------------------
        public UserInputDialog()
        {
            InitializeComponent();
        }

        // -----------------------------------------------------------------------------
        // Triggers
        // -----------------------------------------------------------------------------
        private void btnCancel_Click(object sender, EventArgs e)
        {
            // Clears the info on the user input form and hides screen
            ShipData = new Ships();
            this.Close();
        }

        // OK button to add to listview
        private void btnOK_Click(object sender, EventArgs e)
        {
            // Make sure the user has input values in all user input controls
            if (tbName.Text != "" && (rbCruiser.Checked == true || rbDestroyer.Checked == true ||
                rbFreighter.Checked == true) && numCrewSize.Value != 0)
            {
                // If statement to catch the null exception
                if (ObjectAdded != null)
                {
                    // Sending the new object to the list-view
                    ObjectAdded(this, new EventArgs());
                }

                // Clearing the input fields and hiding the window
                ShipData = new Ships();
                this.Close();
            }
            else
            {
                // Message box to show what is going on and to update the user
                MessageBox.Show("Please fill out all information\r\nOr hit the cancel button to close.", "Need info",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            // Make sure the user has not left anything blank
            if (tbName.Text != "" && (rbCruiser.Checked == true || rbDestroyer.Checked == true ||
                rbFreighter.Checked == true) && numCrewSize.Value != 0)
            {
                // Call to the modified object change
                main.ModifyObject = new EventHandler<MainWindow.ModifyObjectEventArgs>(this.HandleModifyObject);

                // invoke the modified object event
                main.ModifyObjectEvent();

                // Close the form after the update has taken place
                this.Close();
            }
            else
            {
                // Show the user what happened and why there is an error
                MessageBox.Show("Please fill out all information\r\nOr hit the cancel button to close.", "Need info",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        // -----------------------------------------------------------------------------
        // Object Handlers
        // -----------------------------------------------------------------------------
        // Selected handler
        public void SelectionChangedHandler(object sender, EventArgs e)
        {
            // Setting the main window as the sender
            main = sender as MainWindow;

            // Throwing the values that the list view has stored
            ShipData = main.SelectedObject;
        }

        // Handler for the modified object
        public void HandleModifyObject(object sender, MainWindow.ModifyObjectEventArgs e)
        {
            // Set the tag as new data inputs
            Ships s = e.ObjectToModify1.Tag as Ships;
            s.Name = tbName.Text;
            s.Cruiser = rbCruiser.Checked;
            s.Destroyer = rbDestroyer.Checked;
            s.Freighter = rbFreighter.Checked;
            s.CrewSize = numCrewSize.Value;
            s.ActiveDuty = cbActiveDuty.Checked;
            // Change the image index if the user has selected a different ship
            if (rbCruiser.Checked == true)
            {
                s.ImageIndex = 0;
            }
            else if (rbDestroyer.Checked == true)
            {
                s.ImageIndex = 1;
            }
            else if (rbFreighter.Checked == true)
            {
                s.ImageIndex = 2;
            }
            // Call to the modified object class
            e.ObjectToModify1.Text = s.ToString();
            e.ObjectToModify1.ImageIndex = s.ImageIndex;
        }

        // -----------------------------------------------------------------------------
        // Methods
        // -----------------------------------------------------------------------------
        // Update button method
        public void UpdateButton(bool i)
        {
            // Make the okay button not visible
            btnOK.Visible = false;
            // Change the apply button to what is entered
            btnApply.Visible = i;
        }

    }
}
