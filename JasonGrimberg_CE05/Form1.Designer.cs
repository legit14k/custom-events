﻿namespace JasonGrimberg_CE05
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuClear = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuShip = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuView = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLarge = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSmall = new System.Windows.Forms.ToolStripMenuItem();
            this.ilLargeImage = new System.Windows.Forms.ImageList(this.components);
            this.ilSmallImage = new System.Windows.Forms.ImageList(this.components);
            this.mainStatus = new System.Windows.Forms.StatusStrip();
            this.tsItems = new System.Windows.Forms.ToolStripStatusLabel();
            this.lvMainWindow = new System.Windows.Forms.ListView();
            this.menuMain.SuspendLayout();
            this.mainStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuMain
            // 
            this.menuMain.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuShip,
            this.menuView});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Padding = new System.Windows.Forms.Padding(4, 1, 0, 1);
            this.menuMain.Size = new System.Drawing.Size(284, 24);
            this.menuMain.TabIndex = 3;
            this.menuMain.Text = "MainMenu";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuClear,
            this.menuExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(37, 22);
            this.menuFile.Text = "&File";
            // 
            // menuClear
            // 
            this.menuClear.Name = "menuClear";
            this.menuClear.Size = new System.Drawing.Size(101, 22);
            this.menuClear.Text = "&Clear";
            this.menuClear.Click += new System.EventHandler(this.menuClear_Click);
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.Size = new System.Drawing.Size(101, 22);
            this.menuExit.Text = "E&xit";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // menuShip
            // 
            this.menuShip.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNew});
            this.menuShip.Name = "menuShip";
            this.menuShip.Size = new System.Drawing.Size(42, 22);
            this.menuShip.Text = "&Ship";
            // 
            // menuNew
            // 
            this.menuNew.Name = "menuNew";
            this.menuNew.Size = new System.Drawing.Size(98, 22);
            this.menuNew.Text = "&New";
            this.menuNew.Click += new System.EventHandler(this.menuNew_Click);
            // 
            // menuView
            // 
            this.menuView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuLarge,
            this.menuSmall});
            this.menuView.Name = "menuView";
            this.menuView.Size = new System.Drawing.Size(44, 22);
            this.menuView.Text = "&View";
            // 
            // menuLarge
            // 
            this.menuLarge.CheckOnClick = true;
            this.menuLarge.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.menuLarge.Name = "menuLarge";
            this.menuLarge.Size = new System.Drawing.Size(152, 22);
            this.menuLarge.Text = "&Large";
            this.menuLarge.Click += new System.EventHandler(this.menuLarge_Click);
            // 
            // menuSmall
            // 
            this.menuSmall.CheckOnClick = true;
            this.menuSmall.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.menuSmall.Name = "menuSmall";
            this.menuSmall.Size = new System.Drawing.Size(152, 22);
            this.menuSmall.Text = "S&mall";
            this.menuSmall.Click += new System.EventHandler(this.menuSmall_Click);
            // 
            // ilLargeImage
            // 
            this.ilLargeImage.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLargeImage.ImageStream")));
            this.ilLargeImage.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLargeImage.Images.SetKeyName(0, "cruiserLarge.jpg");
            this.ilLargeImage.Images.SetKeyName(1, "destroyerLarge.jpg");
            this.ilLargeImage.Images.SetKeyName(2, "freighterLarge.jpg");
            // 
            // ilSmallImage
            // 
            this.ilSmallImage.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmallImage.ImageStream")));
            this.ilSmallImage.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmallImage.Images.SetKeyName(0, "cruiserSmall.jpg");
            this.ilSmallImage.Images.SetKeyName(1, "destroyerSmall.jpg");
            this.ilSmallImage.Images.SetKeyName(2, "freighterSmall.jpg");
            // 
            // mainStatus
            // 
            this.mainStatus.BackColor = System.Drawing.Color.White;
            this.mainStatus.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsItems});
            this.mainStatus.Location = new System.Drawing.Point(0, 340);
            this.mainStatus.Name = "mainStatus";
            this.mainStatus.Padding = new System.Windows.Forms.Padding(1, 0, 9, 0);
            this.mainStatus.Size = new System.Drawing.Size(284, 22);
            this.mainStatus.TabIndex = 4;
            this.mainStatus.Text = "MainStatus";
            // 
            // tsItems
            // 
            this.tsItems.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tsItems.Name = "tsItems";
            this.tsItems.Size = new System.Drawing.Size(46, 17);
            this.tsItems.Text = "Count: ";
            // 
            // lvMainWindow
            // 
            this.lvMainWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvMainWindow.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvMainWindow.LargeImageList = this.ilLargeImage;
            this.lvMainWindow.Location = new System.Drawing.Point(0, 24);
            this.lvMainWindow.Margin = new System.Windows.Forms.Padding(2);
            this.lvMainWindow.Name = "lvMainWindow";
            this.lvMainWindow.Size = new System.Drawing.Size(284, 316);
            this.lvMainWindow.SmallImageList = this.ilSmallImage;
            this.lvMainWindow.TabIndex = 5;
            this.lvMainWindow.UseCompatibleStateImageBehavior = false;
            this.lvMainWindow.View = System.Windows.Forms.View.SmallIcon;
            this.lvMainWindow.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvMainWindow_MouseDoubleClick);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(284, 362);
            this.Controls.Add(this.lvMainWindow);
            this.Controls.Add(this.mainStatus);
            this.Controls.Add(this.menuMain);
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ship Controller";
            this.Activated += new System.EventHandler(this.MainWindow_Activated);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.mainStatus.ResumeLayout(false);
            this.mainStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuClear;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.ToolStripMenuItem menuShip;
        private System.Windows.Forms.ToolStripMenuItem menuNew;
        private System.Windows.Forms.ToolStripMenuItem menuView;
        private System.Windows.Forms.ToolStripMenuItem menuLarge;
        private System.Windows.Forms.ToolStripMenuItem menuSmall;
        private System.Windows.Forms.ImageList ilLargeImage;
        private System.Windows.Forms.ImageList ilSmallImage;
        private System.Windows.Forms.StatusStrip mainStatus;
        private System.Windows.Forms.ToolStripStatusLabel tsItems;
        public System.Windows.Forms.ListView lvMainWindow;
    }
}

