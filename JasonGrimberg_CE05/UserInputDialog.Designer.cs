﻿namespace JasonGrimberg_CE05
{
    partial class UserInputDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbShipInfo = new System.Windows.Forms.GroupBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblCrewSize = new System.Windows.Forms.Label();
            this.lblShipType = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.cbActiveDuty = new System.Windows.Forms.CheckBox();
            this.numCrewSize = new System.Windows.Forms.NumericUpDown();
            this.tbName = new System.Windows.Forms.TextBox();
            this.rbFreighter = new System.Windows.Forms.RadioButton();
            this.rbDestroyer = new System.Windows.Forms.RadioButton();
            this.rbCruiser = new System.Windows.Forms.RadioButton();
            this.gbShipInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCrewSize)).BeginInit();
            this.SuspendLayout();
            // 
            // gbShipInfo
            // 
            this.gbShipInfo.Controls.Add(this.btnApply);
            this.gbShipInfo.Controls.Add(this.btnCancel);
            this.gbShipInfo.Controls.Add(this.btnOK);
            this.gbShipInfo.Controls.Add(this.lblCrewSize);
            this.gbShipInfo.Controls.Add(this.lblShipType);
            this.gbShipInfo.Controls.Add(this.lblName);
            this.gbShipInfo.Controls.Add(this.cbActiveDuty);
            this.gbShipInfo.Controls.Add(this.numCrewSize);
            this.gbShipInfo.Controls.Add(this.tbName);
            this.gbShipInfo.Controls.Add(this.rbFreighter);
            this.gbShipInfo.Controls.Add(this.rbDestroyer);
            this.gbShipInfo.Controls.Add(this.rbCruiser);
            this.gbShipInfo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbShipInfo.Location = new System.Drawing.Point(11, 11);
            this.gbShipInfo.Margin = new System.Windows.Forms.Padding(2);
            this.gbShipInfo.Name = "gbShipInfo";
            this.gbShipInfo.Padding = new System.Windows.Forms.Padding(2);
            this.gbShipInfo.Size = new System.Drawing.Size(262, 240);
            this.gbShipInfo.TabIndex = 1;
            this.gbShipInfo.TabStop = false;
            this.gbShipInfo.Text = "Ship Specs";
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(199, 199);
            this.btnApply.Margin = new System.Windows.Forms.Padding(2);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(59, 37);
            this.btnApply.TabIndex = 8;
            this.btnApply.Text = "&Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Visible = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(4, 199);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(59, 37);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(199, 199);
            this.btnOK.Margin = new System.Windows.Forms.Padding(2);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(59, 37);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblCrewSize
            // 
            this.lblCrewSize.AutoSize = true;
            this.lblCrewSize.Location = new System.Drawing.Point(46, 131);
            this.lblCrewSize.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCrewSize.Name = "lblCrewSize";
            this.lblCrewSize.Size = new System.Drawing.Size(75, 18);
            this.lblCrewSize.TabIndex = 4;
            this.lblCrewSize.Text = "Crew Size: ";
            // 
            // lblShipType
            // 
            this.lblShipType.AutoSize = true;
            this.lblShipType.Location = new System.Drawing.Point(77, 63);
            this.lblShipType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblShipType.Name = "lblShipType";
            this.lblShipType.Size = new System.Drawing.Size(44, 18);
            this.lblShipType.TabIndex = 4;
            this.lblShipType.Text = "Type: ";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(69, 34);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(52, 18);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "Name: ";
            // 
            // cbActiveDuty
            // 
            this.cbActiveDuty.AutoSize = true;
            this.cbActiveDuty.Location = new System.Drawing.Point(125, 159);
            this.cbActiveDuty.Margin = new System.Windows.Forms.Padding(2);
            this.cbActiveDuty.Name = "cbActiveDuty";
            this.cbActiveDuty.Size = new System.Drawing.Size(97, 22);
            this.cbActiveDuty.TabIndex = 5;
            this.cbActiveDuty.Text = "Active Duty";
            this.cbActiveDuty.UseVisualStyleBackColor = true;
            // 
            // numCrewSize
            // 
            this.numCrewSize.Location = new System.Drawing.Point(125, 129);
            this.numCrewSize.Margin = new System.Windows.Forms.Padding(2);
            this.numCrewSize.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numCrewSize.Name = "numCrewSize";
            this.numCrewSize.Size = new System.Drawing.Size(133, 26);
            this.numCrewSize.TabIndex = 4;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(125, 31);
            this.tbName.Margin = new System.Windows.Forms.Padding(2);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(133, 26);
            this.tbName.TabIndex = 0;
            // 
            // rbFreighter
            // 
            this.rbFreighter.AutoSize = true;
            this.rbFreighter.Location = new System.Drawing.Point(135, 103);
            this.rbFreighter.Margin = new System.Windows.Forms.Padding(2);
            this.rbFreighter.Name = "rbFreighter";
            this.rbFreighter.Size = new System.Drawing.Size(83, 22);
            this.rbFreighter.TabIndex = 3;
            this.rbFreighter.TabStop = true;
            this.rbFreighter.Text = "Freighter";
            this.rbFreighter.UseVisualStyleBackColor = true;
            // 
            // rbDestroyer
            // 
            this.rbDestroyer.AutoSize = true;
            this.rbDestroyer.Location = new System.Drawing.Point(135, 82);
            this.rbDestroyer.Margin = new System.Windows.Forms.Padding(2);
            this.rbDestroyer.Name = "rbDestroyer";
            this.rbDestroyer.Size = new System.Drawing.Size(87, 22);
            this.rbDestroyer.TabIndex = 2;
            this.rbDestroyer.TabStop = true;
            this.rbDestroyer.Text = "Destroyer";
            this.rbDestroyer.UseVisualStyleBackColor = true;
            // 
            // rbCruiser
            // 
            this.rbCruiser.AutoSize = true;
            this.rbCruiser.Location = new System.Drawing.Point(135, 61);
            this.rbCruiser.Margin = new System.Windows.Forms.Padding(2);
            this.rbCruiser.Name = "rbCruiser";
            this.rbCruiser.Size = new System.Drawing.Size(70, 22);
            this.rbCruiser.TabIndex = 1;
            this.rbCruiser.TabStop = true;
            this.rbCruiser.Text = "Cruiser";
            this.rbCruiser.UseVisualStyleBackColor = true;
            // 
            // UserInputDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.ControlBox = false;
            this.Controls.Add(this.gbShipInfo);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(300, 300);
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "UserInputDialog";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UserInputDialog";
            this.gbShipInfo.ResumeLayout(false);
            this.gbShipInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCrewSize)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbShipInfo;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblCrewSize;
        private System.Windows.Forms.Label lblShipType;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.CheckBox cbActiveDuty;
        private System.Windows.Forms.NumericUpDown numCrewSize;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.RadioButton rbFreighter;
        private System.Windows.Forms.RadioButton rbDestroyer;
        private System.Windows.Forms.RadioButton rbCruiser;
    }
}