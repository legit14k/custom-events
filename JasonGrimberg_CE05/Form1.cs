﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/// <summary>
/// Jason Grimberg
/// Coding exercise 05 Custom Events
/// 1 - Define a delegate
/// 2 - Define an event based on that delegate
/// 3 - Raise the event
/// </summary>
namespace JasonGrimberg_CE05
{
    public partial class MainWindow : Form
    {
        // -----------------------------------------------------------------------------
        // EventHandler call
        // -----------------------------------------------------------------------------
        // Add delegate for the modified object
        // Call to the class
        public EventHandler<ModifyObjectEventArgs> ModifyObject;

        // Event handler for the selection changed
        public EventHandler SelectionChanged;

        // -----------------------------------------------------------------------------
        // Classes
        // -----------------------------------------------------------------------------
        // Class for the modified object event
        // inherits from the Eventargs event
        public class ModifyObjectEventArgs : EventArgs
        {
            ListViewItem ObjectToModify;

            // The object to modify
            public ListViewItem ObjectToModify1
            {
                // Get the new modified object
                get
                {
                    return ObjectToModify;
                }

                // Set the new modified object
                set
                {
                    ObjectToModify = value;
                }
            }

            // Setting the new modified object to the selected listview item
            public ModifyObjectEventArgs(ListViewItem lvi)
            {
                ObjectToModify = lvi;
            }
        }

        // -----------------------------------------------------------------------------
        // Initialization of the component
        // -----------------------------------------------------------------------------
        public MainWindow()
        {
            InitializeComponent();

            // Make the default display icons small
            menuSmall.Checked = true;
            menuSmall.Enabled = false;
        }

        // -----------------------------------------------------------------------------
        // Triggers
        // -----------------------------------------------------------------------------
        // Main window activated trigger
        private void MainWindow_Activated(object sender, EventArgs e)
        {
            // Change the count text the how many objects are in the list view
            tsItems.Text = "Count: " + lvMainWindow.Items.Count.ToString();
        }

        // Clear list 
        private void menuClear_Click(object sender, EventArgs e)
        {
            // Clear the list view and reset the count
            lvMainWindow.Clear();
            tsItems.Text = "Count: " + lvMainWindow.Items.Count.ToString();
        }

        // Exit menu control
        private void menuExit_Click(object sender, EventArgs e)
        {
            // Close out of the application
            Application.Exit();
        }

        // New window menu control
        private void menuNew_Click(object sender, EventArgs e)
        {
            // New instance of the user input window
            UserInputDialog uid = new UserInputDialog();

            // Sub to the event handler for the object added
            uid.ObjectAdded = new EventHandler(this.ObjectAddedHandler);

            // Show a new user input window
            uid.ShowDialog();
        }

        // Large icon menu control
        private void menuLarge_Click(object sender, EventArgs e)
        {
            // Make sure that the checks are in the right sopt
            menuSmall.Checked = false;
            menuLarge.Enabled = false;

            // Change the icons
            if (menuLarge.Checked == true)
            {
                // Change to large icons
                lvMainWindow.View = View.LargeIcon;
                // Make sure that the small menu check is false
                menuSmall.Enabled = true;
            }
        }

        // Small icon menu control
        private void menuSmall_Click(object sender, EventArgs e)
        {
            // Make sure that the checks are in the right spot
            menuLarge.Checked = false;
            menuSmall.Enabled = false;

            // Change the icons
            if (menuSmall.Checked == true)
            {
                // Change to small icons
                lvMainWindow.View = View.SmallIcon;
                // Make sure that the large menu check is false
                menuLarge.Enabled = true;
            }
        }

        // Double click event that will trigger the current selection
        private void lvMainWindow_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // New instance of the user input form
            UserInputDialog uid = new UserInputDialog();

            // Set the selection to the new window
            SelectionChanged = new EventHandler(uid.SelectionChangedHandler);

            // Sub to the modified handle event
            ModifyObject += uid.HandleModifyObject;

            // Invoke the selection to call the new data coming into the new window
            SelectionChanged?.Invoke(this, new EventArgs());

            // Send the update button to true
            uid.UpdateButton(true);

            // Show the user input data window
            uid.ShowDialog();
        }

        // -----------------------------------------------------------------------------
        // Object Handlers
        // -----------------------------------------------------------------------------
        // New ships when they are modified
        public Ships SelectedObject
        {
            // Just getting the information from the selected item
            get
            {
                // Error protection if there are no items to select
                if (lvMainWindow.SelectedItems.Count > 0)
                {
                    // Will return information if there is more than
                    // zero in the tag and will return only at index zero
                    return lvMainWindow.SelectedItems[0].Tag as Ships;
                }
                else
                {
                    return new Ships();
                }
            }
        }

        // New object added handler
        public void ObjectAddedHandler(object sender, EventArgs e)
        {
            // User input form will be the sender
            // Ships will have for data and all text values
            // Calling a new instance of the list-view items
            UserInputDialog uid = sender as UserInputDialog;
            Ships s = uid.ShipData;
            ListViewItem lvi = new ListViewItem();

            // Setting all information to a string
            // Setting the image to its proper index
            // Setting the tag the to the ship information
            lvi.Text = s.ToString();
            lvi.ImageIndex = s.ImageIndex;
            lvi.Tag = s;

            // Adding the new ship and all of its information
            // to the list-view form
            lvMainWindow.Items.Add(lvi);
        }

        // -----------------------------------------------------------------------------
        // Methods
        // -----------------------------------------------------------------------------
        // Modify object event method
        public void ModifyObjectEvent()
        {
            // Make sure the the object is not null and that an object is selected
            if (ModifyObject != null && lvMainWindow.SelectedItems.Count > 0)
            {
                // Modify the selected object
                ModifyObject(this, new ModifyObjectEventArgs(lvMainWindow.SelectedItems[0]));
            }
        }

        
    }
}
