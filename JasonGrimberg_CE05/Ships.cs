﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Jason Grimberg
/// CE05 
/// Ships class to hold all data
/// </summary>
namespace JasonGrimberg_CE05
{
    public class Ships
    {
        // All ship class variables
        string _name;
        bool _cruiser;
        bool _destroyer;
        bool _freighter;
        decimal _crewSize;
        bool _activeDuty;
        int _imageIndex;
        string shipType;
        string active;

        // New constructors, getters, and setters
        public string Name { get => _name; set => _name = value; }
        public bool Cruiser { get => _cruiser; set => _cruiser = value; }
        public bool Destroyer { get => _destroyer; set => _destroyer = value; }
        public bool Freighter { get => _freighter; set => _freighter = value; }
        public decimal CrewSize { get => _crewSize; set => _crewSize = value; }
        public bool ActiveDuty { get => _activeDuty; set => _activeDuty = value; }
        public int ImageIndex { get => _imageIndex; set => _imageIndex = value; }

        // Override function to display new list items in list-view form and 
        // on main window list
        public override string ToString()
        {
            // Choose what picture index to show
            if (Cruiser == true)
            {
                shipType = "Cruiser";
            }
            else if (Destroyer == true)
            {
                shipType = "Destroyer";
            }
            else if (Freighter == true)
            {
                shipType = "Freighter";
            }

            // Choose whether this is an active duty ship or not
            if (ActiveDuty == true)
            {
                active = "Yes";
            }
            else
            {
                active = "No";
            }
            // Return some information on the ship that was created or selected
            return ("*Name: " + Name.ToString().Trim() + "\r\n *Crew: " + CrewSize.ToString().Trim() +
                "\r\n *Active: " + active.ToString().Trim());

            // Having the ship type in the description seems to be a little redundant
            //" Ship: " + shipType.ToString().Trim() +
        }
    }
}
